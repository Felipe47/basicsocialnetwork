export const environment = {
  production: true,
  // Declaramos nuestro ambiente apuntando a nuestra API de la misma que nuestra conexión a firebae para las imágenes.
  serverUrl: 'https://gorest.co.in/public-api/',
  firebase: {
    apiKey: 'AIzaSyAiD2JEV3NXKKPSM2aKrdklBCZkGrjtWcQ',
    authDomain: 'gluky-upload-image.firebaseapp.com',
    databaseURL: 'https://gluky-upload-image.firebaseio.com',
    projectId: 'gluky-upload-image',
    storageBucket: 'gluky-upload-image.appspot.com',
    messagingSenderId: '610642602016',
    appId: '1:610642602016:web:efeec03e55baabae77f445'
  }
};
