import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Full_ROUTES } from './shared/routes/full-layout.routes';
import { LayoutComponent } from './layout/layout.component';

// Estrategia de carga dinámica de nuestras vistas, acá podremos instanciar nuestros módulos de la aplicación.
const routes: Routes = [
  // En primera instancia cargamos nuestra plantilla hecha en el AppComponent.html, ésta tendra nuestro router-outlet.
  { path: '', component: LayoutComponent, children: Full_ROUTES },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
