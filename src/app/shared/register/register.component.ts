import { UserModel } from './../models/user-model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../API/user.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
// Usamos Sweetalert para los mensajes emergentes
import Swal from 'sweetalert2';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: UserModel = new UserModel();

  constructor(private userService: UserService, private router: Router) { }

  @ViewChild('f', { static: false }) userForm: NgForm;


  ngOnInit(): void {
  }

  // Registro de usuario
  createUser() {
    const endpoint = 'users';
    this.userService.post(endpoint, this.user).subscribe(data => {
      if (data._meta.code === 200) {
        alert('data que llega kelly ' + JSON.stringify(data));
        Swal.fire('Bienvenido ' + data.result.first_name, 'Usuario Registrado con éxito', 'success');
        this.router.navigate(['/shared/login']);
      } else {
        Swal.fire('Ha ocurrido un error ', data.result.message, 'error');
      }
    }, error => {
      Swal.fire('ERROR DE CONEXIÓN', error, 'error');
    });
  }

}
