import { Component, OnInit } from '@angular/core';
import { UserService } from '../API/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  loggedIn() {
    if (this.userService.isAuthenticated()) {
      return true;
    } else {
      return false;
    }
  }

  signOut() {
    window.sessionStorage.clear();
    this.router.navigate(['/']);
  }

}
