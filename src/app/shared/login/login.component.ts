import { TokenStorageService } from './../API/token-storage.service';
import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../models/login-model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

declare var require: any;
const data: any = require('../../shared/data/user.json');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginModel: LoginModel = new LoginModel();

  constructor(private router: Router, private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
  }

  // Dentro de este método vamos a simular una autenticación, leemos un JSON personalizado que se encontrará en el
  // directorio "data", y a partir de esto, haremos la autenticación, guardaremos el id en nuestro servicio de
  // tokenStorage.
  login() {
    for (const i in data.response) {
      if (this.loginModel.email === data.response[i].email) {
        // Empezamos a guardar nuestros datos en nuestro servicio de tokenStorage:
        this.tokenStorage.saveID(data.response[i].id);
        this.tokenStorage.saveUsername(data.response[i].first_name);
        this.tokenStorage.saveEMAIL(data.response[i].email);
        Swal.fire('Bienvenido ' + this.loginModel.email, 'Sesión iniciada correctamente', 'success');
        this.router.navigate(['/']);
        return;
      }
    }
    Swal.fire('Error de inicio de sesión', 'Usuario y/ó Contraseña incorrectas.', 'error');
  }
}
