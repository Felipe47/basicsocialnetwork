import { UserModel } from './../models/user-model';
import { environment } from './../../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TokenStorageService } from './token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private httpHeaders = new HttpHeaders({
    Accept: 'application/json',
    'content-type': 'application/json',
    Authorization: 'Bearer qhKAQLpSKIhvJOTf8q9BkpUILNyoPNLRYR6B'
  });
  private serverURL = environment.serverUrl;

  // Inyectamos nuestras dependencias, el tokenStorage nos servirá para guardar nuestro token en el navegador.
  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  // Servicios:

  get(path, model): Observable<any> {
    return this.http.post<any>(`${this.serverURL}${path}?page=5`, model, { headers: this.httpHeaders });
  }

  getById(path, id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}${path}${'/'}${id}`, { headers: this.httpHeaders });
  }

  post(path, model): Observable<any> {
    return this.http.post<any>(`${this.serverURL}${path}`, model, { headers: this.httpHeaders }).pipe(
      map(response => response as UserModel)
    );
  }

  put(path, id, model): Observable<any> {
    return this.http.put<any>(`${this.serverURL}${path}${'/'}${id}`, model, { headers: this.httpHeaders })
  }

  delete(path, id): Observable<any> {
    return this.http.delete<any>(`${this.serverURL}${path}${'/'}${id}`);
  }

  isAuthenticated() {
    return !!this.tokenStorage.getID();
  }


  // Indicamos al servicio qué tipo de header debe usar en el momento.
  getHeaders() {
    let header = null;
    if (this.tokenStorage.getToken() != null) {
      header = {
        authorization: this.tokenStorage.getToken()
      };
    } else {
      header = {
        // tslint:disable-next-line: max-line-length
        authorization: 'Basic c3ByaW5nLXNlY3VyaXR5LW9hdXRoMi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=='
      };
    }
    return { headers: new HttpHeaders(header) };
  }
}




