import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { TokenStorageService } from './token-storage.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostModel } from '../models/post-model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private httpHeaders = new HttpHeaders({
    Authorization: 'Bearer qhKAQLpSKIhvJOTf8q9BkpUILNyoPNLRYR6B'
  });
  private serverURL = environment.serverUrl;

  // Inyectamos nuestras dependencias, el tokenStorage nos servirá para guardar nuestro token en el navegador.
  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  // Servicios:

  get(endpoint): Observable<any> {
    return this.http.get<any>(`${this.serverURL}${endpoint}`, { headers: this.httpHeaders });
  }

  getByUserId(path, id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}${path}${'?user_id='}${id}`, { headers: this.httpHeaders });
  }
  getById(path, id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}${path}${'/'}${id}`, { headers: this.httpHeaders });
  }

  post(path, model): Observable<any> {
    return this.http.post<any>(`${this.serverURL}${path}`, model, { headers: this.httpHeaders }).pipe(
      map(response => response as PostModel)
    );
  }

  put(path, id, model): Observable<any> {
    return this.http.put<any>(`${this.serverURL}${path}${'/'}${id}`, model, { headers: this.httpHeaders });
  }

  delete(path, id): Observable<any> {
    return this.http.delete<any>(`${this.serverURL}${path}${'/'}${id}`, { headers: this.httpHeaders });
  }

  comment_post(path, model): Observable<any> {
    return this.http.post<any>(`${this.serverURL}${path}`, model, { headers: this.httpHeaders }).pipe(
      map(response => response as PostModel)
    );
  }

  getCommentsByPostId(path, id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}${path}${'?post_id='}${id}`, { headers: this.httpHeaders });
  }
}
