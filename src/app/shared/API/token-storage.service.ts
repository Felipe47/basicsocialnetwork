import { Injectable } from '@angular/core';

const TOKEN_KEY = 'Acess_Token';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';
const AUTHORITIES_ID = 'AuthId';
const EMAIL = 'Email';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

   // Función que nos permite borrar el token que tengamos en el navegador en ese momento.
   signOut() {
    window.sessionStorage.clear();
  }
  // Función que nos permite guardar nuestro token, pero para ello, nos aseguramos en remover cualquier token que haya en el momento.
  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }
  // Recoger nuestro token:
  public getToken() {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  // FUNCIONES DE GUARDADO DE DATOS EN EL NAVEGADOR:

  // Username:
  public saveUsername(username: string) {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY, username);
  }
  public getUsername() {
    return sessionStorage.getItem(USERNAME_KEY)
  }
   // ID:
   public saveID(id: string) {
    window.sessionStorage.removeItem(AUTHORITIES_ID);
    window.sessionStorage.setItem(AUTHORITIES_ID, id);
  }
  public getID() {
    return sessionStorage.getItem(AUTHORITIES_ID);
  }
   // EMAIL:
   public saveEMAIL(email: string) {
    window.sessionStorage.removeItem(EMAIL);
    window.sessionStorage.setItem(EMAIL, email);
  }
  public getEMAIL() {
    return sessionStorage.getItem(EMAIL);
  }
}
