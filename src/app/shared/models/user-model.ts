// Modelo del usuario
export class UserModel {
  first_name: string;
  last_name: string;
  gender: string;
  email: string;
  phone: string;
  status = 'active';
  constructor() {}
}
