// Dentro de este archivo se encontrarán las rutas que serán cargadas a través de LazyLoading de Angular.

import { Routes } from '@angular/router';

// tslint:disable-next-line: variable-name
export const Full_ROUTES: Routes = [
  // homePage Module
  {
    path: '',
    loadChildren: './home/home.module#HomeModule'
  },
  // Shared Module
  {
    path: 'shared',
    loadChildren: './shared/shared.module#SharedModule'
  },
  // User Module
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  },
  // Post
  {
    path: 'post',
    loadChildren: './post/post.module#PostModule'
  }
];
