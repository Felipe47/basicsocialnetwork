// tslint:disable-next-line: max-line-length
// Esto es lo que nos permitirá hacer la guardia de rutas dentro de nuestra aplicación, haciendo que sólo los usuarios logueados tengan acceso.

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './shared/API/user.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router) { }

  canActivate(): boolean {
    if (this.userService.isAuthenticated()) {
      return true;
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Por favor, inicia sesión'
      });
      this.router.navigate(['/shared/login']);
      return false;
    }
  }
}
