import { ViewComponent } from './view/view.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePostComponent } from './create-post/create-post.component';
import { ListPostComponent } from './list-post/list-post.component';
import { UpdatePostComponent } from './update-post/update-post.component';
import { AuthGuard } from '../auth.guard';

// Enrutamiento de nuestro de módulo Post, acá además de enrutar a nuestros componentes
// le diremos también qué es una ruta que está protegida por una guardia.

const routes: Routes = [
  {
    path: 'create',
    component: CreatePostComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'list',
    component: ListPostComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update',
    component: UpdatePostComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'view',
    component: ViewComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
