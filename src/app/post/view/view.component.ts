import { Component, OnInit } from '@angular/core';
import { PostModel } from 'src/app/shared/models/post-model';
import { PostService } from 'src/app/shared/API/post.service';
import Swal from 'sweetalert2';
import { TokenStorageService } from 'src/app/shared/API/token-storage.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdatePostComponent } from '../update-post/update-post.component';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  post: PostModel = new PostModel();
  habilitar = true;
  downloadURL: any;


  constructor(private postService: PostService, private tokenStorage: TokenStorageService,
    private afStorage: AngularFireStorage, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getbyIdPost(this.tokenStorage.getID());
    // this.downloadURL = this.afStorage.ref('/images/my_file_name.png').getDownloadURL();
    // alert(JSON.stringify(this.downloadURL));
  }

  getbyIdPost(id) {
    const endpoint = 'posts';
    this.postService.getByUserId(endpoint, id).subscribe(
      data => {
        if (data._meta.code === 200) {
          this.post = data.result;
        } else {
          Swal.fire('Error', data.result.message, 'error');
        }
      }, error => {
        Swal.fire('Oops', error, 'error');
      }
    );
  }

  update(id) {
    const modalRef = this.modalService.open(UpdatePostComponent, { windowClass: 'custom-modal', size: 'lg' });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.passEntry.subscribe((receivedData) => {
      this.getbyIdPost(this.tokenStorage.getID());
      Swal.fire('Post Actualizado Correctamente!', receivedData.title, 'success');
      this.modalService.dismissAll();
    });
  }


  delete(id) {
    const endpoint = 'posts';
    Swal.fire({
      title: '¿Está seguro?',
      text: 'No podrá deshacer este cambio luego',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar Post'
    }).then((result) => {
      if (result.value) {
        this.postService.delete(endpoint, id).subscribe(data => {
          if (data._meta.code === 204) {
            this.getbyIdPost(this.tokenStorage.getID());
            Swal.fire(
              'Post Eliminado!',
              'El Post ha sido eliminado satisfactoriamente',
              'success'
            );
          } else {
            Swal.fire('Error', 'no se ha podido eliminar el post', 'error');
          }
        }, error => {
          alert(error);
        });
      }
    });
  }

}
