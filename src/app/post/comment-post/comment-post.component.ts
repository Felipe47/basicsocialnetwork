import { TokenStorageService } from './../../shared/API/token-storage.service';
import { PostModel } from 'src/app/shared/models/post-model';
import { CommentModel } from './../../shared/models/comment-model';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PostService } from 'src/app/shared/API/post.service';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-comment-post',
  templateUrl: './comment-post.component.html',
  styleUrls: ['./comment-post.component.css']
})
export class CommentPostComponent implements OnInit {

  comment: CommentModel = new CommentModel();
  post: PostModel = new PostModel();
  comentarios: any[];
  habilitar = true;
  habilitarBoton = false;

  // Estos decoradores de Angular nos permiten recibir/sacar información desde otros componentes
  @Input() public id;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  constructor(private postService: PostService, private tokenStorage: TokenStorageService) { }

  @ViewChild('f', { static: false }) postForm: NgForm;


  ngOnInit(): void {
    this.getPostInfo();
    this.retrieveComments();
  }

  getPostInfo() {
    const endpoint = 'posts';
    this.postService.getById(endpoint, this.id).subscribe(
      data => {
        if (data._meta.code === 200) {
          this.post = data.result;
        } else {
          Swal.fire('Error', data.result.message, 'error');
        }
      }, error => {
        Swal.fire('Oops', error, 'error');
      });
  }

  retrieveComments() {
    const endpoint = 'comments';

    this.postService.getCommentsByPostId(endpoint, this.id).subscribe(
      data => {
        if (data._meta.code === 200) {
          this.habilitarBoton = true;
          for (const i in data.result) {
            if (data.result[i] !== null) {
              this.comentarios = data.result;
            }
          }
        }
      }, error => {
        Swal.fire('Error', error.message, 'error');
      }
    );
  }

  commentPost() {
    const endpoint = 'comments';
    this.comment = {
      post_id: this.post.id,
      name: this.tokenStorage.getUsername(),
      email: this.tokenStorage.getEMAIL(),
      body: this.comment.body
    };
    this.postService.comment_post(endpoint, this.comment).subscribe(
      data => {
        if (data._meta.code === 200) {
          Swal.fire('Comentario Agregado', data.result.message, 'success');
          this.retrieveComments();
        } else {
          Swal.fire('Ha habido un problema al comentar', data.result.message, 'error');
        }
      }, error => {
        Swal.fire('Error', error.message, 'error');
      }
    );
  }

  delete(id) {
    const endpoint = 'comments';
    this.postService.delete(endpoint, id).subscribe(
      data => {
        if (data._meta.code === 204) {
          this.retrieveComments();
        } else {
          Swal.fire('Error!', 'no se ha eliminado el comentario', 'error');
        }
      }, error => {
        Swal.fire('error', error.message, 'error');
      }
    );

  }

  setHabilitar(): void {
    this.habilitar = (this.habilitar === true) ? false : true;
  }



}
