import { CommentPostComponent } from './../comment-post/comment-post.component';
import { PostService } from './../../shared/API/post.service';
import { Component, OnInit } from '@angular/core';
import { PostModel } from 'src/app/shared/models/post-model';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.css']
})
export class ListPostComponent implements OnInit {

  post: PostModel = new PostModel();
  habilitar = true;
  constructor(private postService: PostService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getAllPost();
  }

  setHabilitar(): void {
    this.habilitar = (this.habilitar === true) ? false : true;
  }
  getAllPost() {
    const endpoint = 'posts';
    this.postService.get(endpoint).subscribe(
      data => {
        if (data._meta.code === 200) {
          this.post = data.result;
        } else {
          Swal.fire('Error', data.result.message, 'error');
        }
      }, error => {
        Swal.fire('Oops', error, 'error');
      }
    );
  }

  comment(id) {
    const modalRef = this.modalService.open(CommentPostComponent, { windowClass: 'custom-modal', size: 'lg' });
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.passEntry.subscribe((receivedData) => {
      // this.getbyIdPost(this.tokenStorage.getID());
      Swal.fire('Post Actualizado Correctamente!', receivedData.title , 'success');
      this.modalService.dismissAll();
    });
  }

}
