import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { CreatePostComponent } from './create-post/create-post.component';
import { UpdatePostComponent } from './update-post/update-post.component';
import { ListPostComponent } from './list-post/list-post.component';
import { FormsModule } from '@angular/forms';
import { ViewComponent } from './view/view.component';
import { CommentPostComponent } from './comment-post/comment-post.component';


@NgModule({
  declarations: [CreatePostComponent, UpdatePostComponent, ListPostComponent, ViewComponent, CommentPostComponent],
  imports: [
    CommonModule,
    PostRoutingModule,
    FormsModule
  ]
})
export class PostModule { }
