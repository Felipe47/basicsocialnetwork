import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { TokenStorageService } from './../../shared/API/token-storage.service';
import { PostService } from './../../shared/API/post.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PostModel } from 'src/app/shared/models/post-model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { map, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  post: PostModel = new PostModel();
  // Para Almacenamiento de Firebase solamente.
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  downloadURL: any;
  uploadState: Observable<string>;

  constructor(private postService: PostService, private afStorage: AngularFireStorage, private tokenStorage: TokenStorageService,
              private router: Router) { }

  @ViewChild('f', { static: false }) postForm: NgForm;

  ngOnInit(): void {
  }

  // Carga de la imágen a firebase
  upload = (event) => {
    // crea un id aleatorio para los índices de firebase
    const randomId = Math.random().toString(36).substring(2);
    // crea una referencia al storage bucket location
    this.ref = this.afStorage.ref('/images/' + randomId);
    // el método put crea una AngularFireUploadTask
    this.task = this.ref.put(event.target.files[0]);

    // AngularFireUploadTask provee un observable
    // para obtener el valor de la uploadProgress bar
    // this.uploadProgress = this.task.snapshotChanges()
    //   .pipe(map(s => (s.bytesTransferred / s.totalBytes) * 100));

    // observe upload progress
    this.uploadProgress = this.task.percentageChanges();
    // get notified when the download URL is available
    this.task.snapshotChanges().pipe(
      finalize(() => this.downloadURL = this.ref.getDownloadURL())
    )
      .subscribe();
    this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
  }

  create() {
    const endpoint = 'posts';
    const postModel = {
      user_id: this.tokenStorage.getID(),
      title: this.post.title,
      body: this.post.body
    };
    this.postService.post(endpoint, postModel).subscribe(
      data => {
        if (data._meta.code === 200) {
          Swal.fire('Post creado!', data._meta.message, 'success');
          this.postForm.reset();
          this.router.navigate(['/post/list']);
        } else {
          Swal.fire('Oops...', data.result.message, 'error');
        }
      }
    );

  }

}
