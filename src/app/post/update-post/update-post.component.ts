import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostService } from 'src/app/shared/API/post.service';
import { PostModel } from 'src/app/shared/models/post-model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css']
})
export class UpdatePostComponent implements OnInit {

  post: PostModel = new PostModel();

  // Estos decoradores de Angular nos permiten recibir/sacar información desde otros componentes
  @Input() public id;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.getPostInfo();
  }

  getPostInfo() {
    const endpoint = 'posts';
    this.postService.getById(endpoint, this.id).subscribe(
      data => {
        if (data._meta.code === 200) {
          this.post = data.result;
        } else {
          Swal.fire('Error', data.result.message, 'error');
        }
      }, error => {
        Swal.fire('Oops', error, 'error');
      });
  }

  update() {
    const path = 'posts';
    const id = this.post.id;
    this.postService.put(path, id, this.post).subscribe(data => {
      this.post = data;
      this.passEntry.emit(this.post);
    }, error => {
      Swal.fire({ icon: 'error', title: 'No se ha podido actualizar el cliente', text: error.message });
    });
  }

}
